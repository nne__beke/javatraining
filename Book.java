public class Book extends Item {
    public Book(String title) {
        super(title);
    }
    public Book(String creatorName, String title) {
        super(creatorName, title);
    }

    public Book(double libNum) {
        super(libNum);
    }

    public Book(long isbn) {
        super(isbn);
    }    
}