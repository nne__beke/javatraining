public class DVD extends Item {
    public DVD(String title) {
        super(title);
    }
    public DVD(String creatorName, String title) {
        super(creatorName, title);
    }

    public DVD(double libNum) {
        super(libNum);
    }    
}