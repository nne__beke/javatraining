public class Periodical extends Item {
    public Periodical(String title) {
        super(title);
    }
    public Periodical(String creatorName, String title) {
        super(creatorName,title);
    }

    public Periodical(double libNum) {
        super(libNum);
    }
}