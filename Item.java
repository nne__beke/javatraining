import java.util.*;

public abstract class Item {
    private String libName;
    private String creatorName;
    private double libNum;
    private String title;
    private double isbn;
    private List<Item> items = new ArrayList<Item>();

    public String getLibName() {
        return libName;
    }

    public void setLibName(String libName) {
        this.libName = libName;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public double getLibNum() {
        return libNum;
    }

    public void setLibNum(double libNum) {
        this.libNum = libNum;
    }

    public Item() {

    }

    public Item(String title) {
        this.title = title;
    }
    public Item(String creatorName, String title) {
        this.creatorName = creatorName;
        this.title = title;
    }

    public Item(double libNum) {
        this.libNum = libNum;
    }

    public Item(long isbn) {
        this.isbn = isbn;
    }    

    public boolean inStock(Item itm)
    {
        if(items.contains(itm)) {
            System.out.println("Item is in stock :)");
            return true;
        }
        else {
            System.out.println("Item is out of stock! Please find something else :(");
            return false;
        }
    }

    public void checkOut(Item itm)
    {
        if(itm instanceof Periodical)
        {
            System.out.println("Periodicals can not be borrowed.");
        }
        else {
            if(inStock(itm)){
                items.remove(itm);
            }
        }
    }

    public void returnIt(Item itm)
    {
        if(!inStock(itm))
        {
            System.out.println("Thank you for returning this item!");
            items.add(itm);
        }
    }

    public void addtoLib(Item itm)
    {
        items.add(itm);
    }
} 