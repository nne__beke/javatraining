public class CD extends Item {
    public CD(String title) {
        super(title);
    }
    public CD(String creatorName, String title) {
        super(creatorName, title);
    }

    public CD(double libNum) {
        super(libNum);
    }
}