import java.util.Scanner;

/* Ideas: Use user input to request and return books based off details they remember.
 * This is a very dumbed-down version of an online library.
 */
public class Library {
    public static void main(String[] args) {
        // creating basic library of books, CDs, DVDs, and periodicals
        Book b1 = new Book("Catcher in the Rye");
        Book b2 = new Book(9780435900267);
    }
    
}